# Deploying a nodejs app to AWS Elastic BeanStalk using GitlabCI


In this tutorial, you'll learn to deploy a nodejs application to AWS ElasticBeanStalk step by step.

**1. Create an AWS Account and assign permissions**

**2. Create a S3 Bucket**

**3. Create the application and the dev and prod environment on ElasticBeanstalk**

**4. Add the environment variables on your Gitlab project repository**


#

**Step 1 : Create an AWS Account and assign permissions**

- Log onto your AWS console and set a new account for Gitlab :

![](img/Capture1.PNG)

- Select "Attach existing policies directly" and choose 
  - AWSElasticBeanstalkFullAccess
  - S3FullAccess

![](img/Capture2.PNG)

- Validate and create the user
![](img/Capture3.PNG)

#

**Step 2 : Create a S3 Bucket**

![](img/Capture11.PNG)
![](img/Capture12.PNG)
![](img/Capture13.PNG)
![](img/Capture14.PNG)
#
**Step 2:    Create the application and the preproduction and production environment on ElasticBeanstalk**

- Create a new application
![](img/Capture4.PNG)

- Name your application (in our example "nodejs-demo-app") and click on "Create"
![](img/Capture5.PNG)

- Then create a new environment "Preproduction"

![](img/Capture6.PNG)

- Select "Web Server environment"
![](img/Capture7.PNG)

- Environment name : "Preproduction"

![](img/Capture8.PNG)

- Select the platform and choose "Sample application for now":
![](img/Capture9.PNG)

- You should have the environments "Preproduction and Production" created and in a healthy state

![](img/Capture10.PNG)


#

**Step 3: Add the environment variables on your Gitlab project repository**

- Select the Gitlab project, then Settings > CICD > Variables

![](img/Capture15.PNG)

- Add the following variables
  - Make sure the AWS_ACCESS_KEY_ID and KEY matches with the gitlabuser we created earlier
  - Make sure the S3_Bucket matches with the S3 Bucket created earlier "nodejs-demo-app"

![](img/Capture16.PNG)